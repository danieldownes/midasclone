package 
{
    import flash.system.System;
    import flash.ui.Keyboard;
    import flash.utils.getDefinitionByName;
	import starling.text.BitmapFont;
	import starling.text.TextField;
    
    import starling.core.Starling;
    import starling.display.Button;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.events.KeyboardEvent;
    import starling.textures.Texture;
    import starling.utils.AssetManager;
    
    import utils.ProgressBar;
	
	import game.GameControl;

    public class MidasClone extends Sprite
    {   
        private var mLoadingProgress:ProgressBar;
        private var mMenu:Menu;
        private var mGameScene:GameControl;
        private var _container:Sprite;
        
        private static var sAssets:AssetManager;
        
        public function MidasClone()
        {
            // nothing to do here -- Startup will call "start" immediately.
        }
        
        public function start(background:Texture, assets:AssetManager):void
        {
            sAssets = assets;
            
            // The background is passed into this method for two reasons:
            // 
            // 1) we need it right away, otherwise we have an empty frame
            // 2) the Startup class can decide on the right image, depending on the device.
            
            addChild(new Image(background));
            
            // The AssetManager contains all the raw asset data, but has not created the textures
            // yet. This takes some time (the assets might be loaded from disk or even via the
            // network), during which we display a progress indicator. 
            
            mLoadingProgress = new ProgressBar(175, 20);
            mLoadingProgress.x = (background.width  - mLoadingProgress.width) * 0.72;
            mLoadingProgress.y = background.height * 0.4;
            addChild(mLoadingProgress);
            
            assets.loadQueue(function(ratio:Number):void
            {
                mLoadingProgress.ratio = ratio;
				
                if (ratio == 1)
                    Starling.juggler.delayCall(function():void
                    {
                        mLoadingProgress.removeFromParent(true);
                        mLoadingProgress = null;
                        showMenu();
                    }, 0.05);
            });
            
            stage.addEventListener(KeyboardEvent.KEY_DOWN, onKey);
        }
        
        private function showMenu(e:Event = null):void
        {
            // now would be a good time for a clean-up 
            System.pauseForGCIfCollectionImminent(0);
            System.gc();
			
            if (mMenu == null)
                mMenu = new Menu(this);
            
            addChild(mMenu);
			mMenu.addEventListener(Menu.PLAY_GAME, playGame);
        }
		
		private function playGame(e:Event):void
        {
			if( mMenu != null)
			{
				mMenu.addEventListener(Menu.PLAY_GAME, playGame);
				removeChild(mMenu);
			}
			
			if (mGameScene == null)
			{
                mGameScene = new GameControl();
				mGameScene.addEventListener(GameControl.GAMEOVER, showMenu);
				addChild(mGameScene);
			}
			else
			{
				mGameScene.reset();
            }
		}
        
        private function onKey(event:KeyboardEvent):void
        {
            if (event.keyCode == Keyboard.SPACE)
                Starling.current.showStats = !Starling.current.showStats;
            else if (event.keyCode == Keyboard.X)
                Starling.context.dispose();
        }
        
		
        public static function get assets():AssetManager
		{
			return sAssets;
		}
    }
}