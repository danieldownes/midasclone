package
{
    public class EmbeddedAssets
    {   
        // Texture Atlas
		[Embed(source="../assets/textures/gems.xml", mimeType="application/octet-stream")]
        public static const gems_xml:Class;
		
		[Embed(source="../assets/textures/gems.png")]
        public static const gems:Class;
		
		// UI
		[Embed(source="../assets/textures/button.png")]
        public static const ButtonTexture:Class;
		
		
		// Bitmap Font
        [Embed(source="../assets/fonts/ArialRoundGrad.fnt", mimeType="application/octet-stream")]
        public static const arialroundgrad_fnt:Class;
        
        [Embed(source = "../assets/fonts/ArialRoundGrad.png")]
        public static const ArialRoundGrad:Class;
		
		// Particals
		[Embed(source="../assets/textures/fire.pex", mimeType="application/octet-stream")]
        public static const FireConfig:Class;
		
		[Embed(source = "../assets/textures/fire_particle.png")]
        public static const FireParticle:Class;
				
		[Embed(source="../assets/textures/hint.pex", mimeType="application/octet-stream")]
        public static const HintConfig:Class;
		
		[Embed(source = "../assets/textures/hint_particle.png")]
        public static const HintParticle:Class;
		
		[Embed(source="../assets/textures/fragment.pex", mimeType="application/octet-stream")]
        public static const FragmentConfig:Class;
		
		[Embed(source = "../assets/textures/fragment_particle.png")]
        public static const FragmentParticle:Class;
		
    }
}