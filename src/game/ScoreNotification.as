package game 
{
	import caurina.transitions.Tweener;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.utils.HAlign;
	
	public class ScoreNotification 
	{
		private var txtNote:TextField;
		private var _game:GameControl;
		
		public function ScoreNotification(game_:GameControl, x_:int, y_:int, score_:int):void
		{
			_game = game_;
			txtNote = new TextField(100, 40, "0", "ArialRoundGrad", 22);
            txtNote.color = Color.WHITE;
            txtNote.x = x_ - 50;
            txtNote.y = y_;
			txtNote.text = score_.toString();
			txtNote.hAlign = HAlign.CENTER;
            game_.addChild(txtNote);
			
			Tweener.addTween(txtNote,
			{ 
				y:y_ - 30,
				alpha: 1,
				time:0.5,
				transition:"easeInQuad"
			} );
			
			Tweener.addTween(txtNote,
			{ 
				y:y_ - 60,
				alpha: 0,
				delay:0.5,
				time:0.5,
				transition:"easeOutQuad",
				onComplete:onTweenComplete
			} );
		}
		
		private function onTweenComplete():void 
		{
			_game.removeChild(txtNote);
		}
	}
}