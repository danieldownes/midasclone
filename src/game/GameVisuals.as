package game 
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import caurina.transitions.Tweener;
	import starling.core.Starling;
	import starling.text.TextField;
	import starling.extensions.ColorArgb;
	import starling.extensions.PDParticleSystem;
	import starling.text.BitmapFont;
    import starling.text.TextField;
    import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	
	public class GameVisuals 
	{
		public var _game:GameControl;
		
		public var txtScore:TextField;
		
		private var _hintTimer:Timer;
		
		private var _gemHint:Gem;
		
		private var hintConfig:XML = XML(new EmbeddedAssets.HintConfig());
		private var hintTexture:Texture = Texture.fromBitmap(new EmbeddedAssets.HintParticle());
		private var fragmentConfig:XML = XML(new EmbeddedAssets.FragmentConfig());
		private var fragmentTexture:Texture = Texture.fromBitmap(new EmbeddedAssets.FragmentParticle());
		private var fragmentColours:Array = new Array(new ColorArgb(0, 0, 1, 1), new ColorArgb(0, 1, 0, 1), new ColorArgb(1, 0, 1, 1), new ColorArgb(1, 0, 0, 1), new ColorArgb(1, 1, 0, 1) );
		
		public function GameVisuals(game_:GameControl) 
		{
			_game = game_;
			
			// Register Bitmap font
			var texture:Texture = Texture.fromBitmap(new EmbeddedAssets.ArialRoundGrad());
			var xml:XML = XML(new EmbeddedAssets.arialroundgrad_fnt());
			TextField.registerBitmapFont(new BitmapFont(texture, xml));
			
			txtScore = new TextField(170, 40, "0", "ArialRoundGrad", 30);
            txtScore.color = Color.WHITE; // use white to use the texture as it is (no tinting)
            txtScore.x = 10;
            txtScore.y = 93;
			txtScore.hAlign = HAlign.RIGHT;
            game_.addChild(txtScore);
			
			_hintTimer = new Timer(6000, 1);
			_hintTimer.addEventListener(TimerEvent.TIMER_COMPLETE, _showHint);
		}
		
		public function setHintGem(gem:Gem = null):void 
		{
			_gemHint = gem;
			_hintTimer.reset();
			_hintTimer.start();
		}
		
		private function _showHint(e:TimerEvent):void 
		{
			if( _gemHint == null)
				return;
			
			var partical:PDParticleSystem = new PDParticleSystem(hintConfig, hintTexture); //hintConfig
			partical.emitterX = _gemHint.x;
			partical.emitterY = _gemHint.y;
			partical.start();
			_game.addChild(partical);
			Starling.juggler.add(partical);
			Tweener.addTween(partical, {time: 1, onComplete:_particalComplete, onCompleteParams:[partical] } );
		}
		
		public function explodeGem(gem:Gem):void
		{
			var particle:PDParticleSystem = new PDParticleSystem(fragmentConfig, fragmentTexture);
			
			// Start
			particle.startColor = fragmentColours[gem.type - 1];
			particle.emitterX = gem.x;
			particle.emitterY = gem.y;
			particle.start();
			_game.addChild(particle);
			Starling.juggler.add(particle);
			
			// Just use Tweener's timer for convenience
			Tweener.addTween(particle, {time: 0.2, onComplete:_particalComplete, onCompleteParams:[particle] } );
		}
		
		private function _particalComplete(particle:PDParticleSystem):void 
		{
			if( particle.x == 0)
			{
				particle.stop();
				// Wait for particles to stop, then remove completely, using x as a flag
				Tweener.addTween(particle, { x:1, time: 3, onComplete:_particalComplete, onCompleteParams:[particle] } );
			}
			else
			{
				particle.stop(true);
				_game.removeChild(particle);
				Starling.juggler.remove(particle);
			}
		}
	}

}