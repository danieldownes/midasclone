package game 
{
	import flash.geom.Point;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class Gem extends Sprite
	{
		public var type:uint; // ref: ("Blue", "Green", "Purple", "Red", "Yellow")
		//private var index:uint;
		public var r:uint, c:uint;	// Row, Column
		private var _game:GameControl;
		private var _image:Image;
		
		public static const SIZE:int = 35;
		public static const GAP:int = 6;
		public static const TYPES:int = 5;
		public static const SWAP_ANI_TIME:Number = 0.3;
		
		// Events fired with gem interaction
		public static const SELECTED:String = "gem_selected";
		public static const MOVED:String = "gem_moved";
		
		public function Gem(game_:GameControl, type_:uint, c_:uint, r_:uint):void
		{
			_game = game_;
			type = type_;
			
			c = c_;
			r = r_;
			
			this.addEventListener(TouchEvent.TOUCH, _onTouch); 
		}
		
		public function loadImage():void
		{
			// Image isn't loaded in constructer, so that type can be changed
			//  after gem is created (when forcing no matches on grid population)
			
			_image = new Image(MidasClone.assets.getTexture("Gem" + type.toString()));
			// Set pivot to middle of image
			_image.x -= SIZE / 2;
			_image.y -= SIZE / 2;
			addChild(_image);
		}
		
		private function _onTouch(e:TouchEvent):void
		{ 
			var touch:Touch = e.getTouch(this);
			
			if( touch != null )
			{
				if( touch.phase == TouchPhase.BEGAN)
				{
					_game.dispatchEventWith(SELECTED, false, { gem:this } );
				}
				else if( touch.phase == TouchPhase.MOVED)
				{
					var m:Point = touch.getMovement(this);
					var dX:int, dY:int;
					
					// Ensure movement is sufficent
					if( Math.abs(m.x) + Math.abs(m.y) < 2)
						return;
					
					// Detect direction
					if( Math.abs(m.x) > Math.abs(m.y))
						dX = ( m.x > 0 ? 1 : -1);
					else
						dY = ( m.y > 0 ? 1 : -1);
					
					_game.dispatchEventWith(MOVED, false, { c:dX, r:dY } );
				}
			}
		} 
	}
}