package game
{
	import caurina.transitions.Tweener;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import starling.core.Starling;
	import starling.display.Sprite;
    import starling.display.Image;
	import starling.events.Event;
	
	import game.*;

    public class GameControl extends Sprite
    {
		public static const ROWS:int = 8;
		public static const COLS:int = 8;
		public static const GRID_X:int = 335;
		public static const GRID_Y:int = 100;
		public static const SECS_PER_ROW:Number = 0.1;
		public static const GAMEOVER:String = "gameover";
		
		public var grid:Array;
		
		private var visuals:GameVisuals;
		
		private var score:int;
		private var score_multiplier:int;
		
		private var _gemSelected:Gem, _gemSwap:Gem;
		private var _canSelect:Boolean;
		
		private var _checkGems:Array;
		private var _gemsDropping:uint;
		
		private var _gameOver:Boolean;
		
		private var _fuse:FuzeCountdown;
		
		
        public function GameControl()
        {
            // Create grid 2d array
			grid = [];
			for (var c:int = 0; c < COLS; c++)
			{
				grid.push([]);
			}
			
			visuals = new GameVisuals(this);
			
			_fuse = new FuzeCountdown(this);
			
			// Mask
			var maskRect:Rectangle = new Rectangle(0, 96, 800, 600);
            this.clipRect = maskRect;
			
			addEventListener(Gem.SELECTED, _onGemSelected);
			addEventListener(Gem.MOVED, _onGemDrag);
			
			reset();
        }
		
		public function reset():void
		{
			_gemSelected = null;
			_gemSwap = null;
			
			_checkGems = null;
			_gemsDropping = 0;
			
			score = 0;
			score_multiplier = 100;
			
			_gameOver = false;
			
			_fuse.reset();
			
			visuals.setHintGem();
			
			_canSelect = false;
			
			populate(true);
		}
		
		/**
		 * Populate the empty grid spaces
		 * Also handles falling and re-population
		 */
		private function populate(preventMatches:Boolean = false, newGems:Array = null):void
		{
			for( var c:int = 0; c < COLS; c++)
			{
				populateCol(c, preventMatches, null, c * 0.1)
			}
		}
		
		/**
		 * Populate column
		 */
		private function populateCol(c:uint, preventMatches:Boolean, newGems:Array = null, delayOffset:Number = 0):void
		{
			var dropped:int = 0;
			
			for(var r:int = ROWS - 1; r >= 0; r--)
			{
				// Add/drop new gem
				if (grid[c][r] == null)
				{
					var type:uint = Math.floor(Math.random() * Gem.TYPES) + 1;
					
					var gem:Gem = new Gem(this, type, c, r);
					
					// Check to ensure new gem wont directly match grid
					if( preventMatches)
					{
						var matchesHorz:Array = new Array();
						var matchesVert:Array = new Array();
						checkMatch(gem, matchesHorz, matchesVert);
						// Note: As long as there are 5 or more gem types, 
						// 		 then an infinite loop should not occur
						while( matchesHorz.length > 1 || matchesVert.length > 1)
						{
							matchesHorz = new Array();
							matchesVert = new Array();
							gem.type++
							if( gem.type > Gem.TYPES)
								gem.type = 1;
							
							checkMatch(gem, matchesHorz, matchesVert);
						}
					}
					
					dropped++;
					
					gem.loadImage();
					gem.x = c * (Gem.SIZE + Gem.GAP) + GRID_X + Gem.SIZE / 2;
					gem.y = (0 - dropped) * (Gem.SIZE + Gem.GAP) + GRID_Y + Gem.SIZE / 2;
					this.addChild(gem);
					
					grid[c][r] = gem;
					
					Tweener.addTween(gem, { x:c * (Gem.SIZE + Gem.GAP) + GRID_X + Gem.SIZE / 2,
											y:r * (Gem.SIZE + Gem.GAP) + GRID_Y + Gem.SIZE / 2,
											time:((r) * SECS_PER_ROW),
											delay:(delayOffset + ((dropped * SECS_PER_ROW)) * 1.4),
											transition:"easeInSine",
											onComplete:(playCheck)
										  } )
					
					// Track new gems
					if( newGems != null)
						newGems.push(gem);
					
					// Don't place at the top of the stake
					//  so that ScoreNotification and other effects
					//  appear over new gems
					if( this.numChildren >= ROWS * COLS)
						this.setChildIndex(gem, ROWS * COLS);
					
					_gemsDropping++;
				}
			}
		}
		
		/**
		 * Checks if gem matches current surrounding gems
		 * Returns: number of matches found during this check
		 */
		private function checkMatch(gem:Gem, matchesHorz:Array, matchesVert:Array):int
		{
			//TODO: may not need matchN
			var matchN:uint = 0;
			matchN += checkDirection( matchesHorz, gem, -1, 0);
			matchN += checkDirection( matchesHorz, gem, 1, 0);
			matchN += checkDirection( matchesVert, gem, 0, -1);
			matchN += checkDirection( matchesVert, gem, 0, 1);
			return matchN;
		}
		
		/**
		 * Checks if gem matches current surrounding gems
		 * Returns: number of matches found during this check
		 */
		private function checkDirection(matches:Array, gem:Gem, c:int, r:int):int
		{
			// Check out of bounds
			if( gem.c + c < 0 || gem.c + c >= GRID_X || gem.r + r < 0 || gem.r + r >= GRID_Y)
				return 0;
			
			// Check for null grid index 
			if( grid[gem.c + c] == null || grid[gem.c + c][gem.r + r] == null )
				return 0;
			
			// Now check for match
			var checkGem:Gem = Gem(grid[gem.c + c][gem.r + r]);
			if( checkGem.type == gem.type)
			{
				matches.push(checkGem);
				return checkDirection(matches, checkGem, c, r) + 1;
			}
			
			return 0;
		}
		
		/**
		 * Drop exiting gems into empty grid spaces
		 */
		private function dropGems(affectedGems_:Array = null):void
		{
			var gem:Gem;
			
			for (var c:int = 0; c < COLS; c++)
			{
				var lR:int = ROWS - 1;		// Last seen row with gem
				var rowDrop:int = 0;		// Number of Gems droped in row
				for (var r:int = ROWS - 1; r >= 0; r--)
				{
					// Row has gem?
					gem = grid[c][r];
					if( gem != null)
					{
						// Drop this gem? Only if no gem below & is not on bottom row
						if( grid[c][r + 1] == null && r != ROWS - 1)
						{
							Tweener.addTween(gem, { x:c * (Gem.SIZE + Gem.GAP) + GRID_X + Gem.SIZE / 2,
													y:lR * (Gem.SIZE + Gem.GAP) + GRID_Y + Gem.SIZE / 2,
													time:(lR - r) * SECS_PER_ROW,
													delay:rowDrop * SECS_PER_ROW * 0.6,
													transition:"easeInSine"
												  } )
							
							// Logical move
							grid[c][lR] = grid[c][r];
							grid[c][r] = null;
							gem.r = lR;
							
							// Save moved gems for checkMatch()
							if( affectedGems_ != null)
								affectedGems_.push(gem);
							
							rowDrop++;
						}
						lR--;
					}
				}
				
				// Repopulate column
				populateCol(c, false, affectedGems_, rowDrop * SECS_PER_ROW * 0.8);
			}
		}
		
		/**
		 * Handle gem swapping using two clicks/touches
		 */
		private function _onGemSelected(e:Event):void
		{
			if( !_canSelect)
				return;
				
			if (_gemSelected == null) 				// Select
			{
				_gemSelected = e.data.gem;
				_gemSelected.scaleX = _gemSelected.scaleY = 1.2;
				// Set selection to top
				setChildIndex(_gemSelected, this.numChildren);
			}
			else if (_gemSelected == e.data.gem) 	// Deselect
			{
				_gemSelected.scaleX = _gemSelected.scaleY = 1;
				_gemSelected = null;
			}
			else 									// Swap
			{
				if( checkSwap(_gemSelected, e.data.gem) )
				{
					_gemSwap = e.data.gem;
					_swapGems(_gemSelected, _gemSwap, _completeSwapAni);
					
					// Disable selection
					_canSelect = false;
				}
				else
				{
					_gemSelected = null;
				}
			}
		}
		
		/**
		 * Handle gem swapping using swipe/drag
		 */
		private function _onGemDrag(e:Event):void 
		{
			if( !_canSelect || _gemSelected == null)
				return;
			
			var c:int = _gemSelected.c + e.data.c;
			var r:int = _gemSelected.r + e.data.r;
			
			// Within bounds?
			if( c >= 0 && c < COLS && r >= 0 && r < ROWS)
			{
				_gemSwap = grid[c][r];
				
				// Gem that is being dragged over is by the side of selected gem?
				if( checkSwap(_gemSelected, _gemSwap))
				{
					_swapGems(_gemSelected, _gemSwap, _completeSwapAni);
					
					// Disable selection
					_canSelect = false;
				}
			}
		}
		
		private function checkSwap(g1:Gem, g2:Gem):Boolean
		{
			return ((g1.c + 1 == g2.c && g1.r == g2.r) ||
					(g1.c - 1 == g2.c && g1.r == g2.r) ||
					(g1.r + 1 == g2.r && g1.c == g2.c) ||
					(g1.r - 1 == g2.r && g1.c == g2.c));
		}
		
		private function playMove():void 
		{
			_checkGems = new Array();
			
			// Drop new gems (and re-populate grid)
			dropGems(_checkGems);
		}
		
		private function playCheck():void
		{
			_gemsDropping--;
			
			if( _gemsDropping > 0)
				return;
			
			_gemsDropping = 0;
			
			// Check and remove any chian matches
			// Pause slightly first, to indicate to player
			Tweener.addCaller(this, { delay:0.2, count:1, onComplete:checkChaining} );
		}
		
		private function checkChaining():void 
		{
			// Check affected gems on grid for new matches
			var matchesN:uint = 0;
			for each( var gem:Gem in _checkGems)
			{
				var n:int = checkAndRemove(gem);
				matchesN += n;
				
				// Display notification for each gem that a match was found
				if ( n > 0)
				{
					var note:ScoreNotification = new ScoreNotification(this, gem.x, gem.y, matchesN * score_multiplier);
				}
			}
			
			if( matchesN > 0 && _checkGems != null && _checkGems.length > 0)
			{
				// Increase score 
				var inc:int = addScore(matchesN * score_multiplier);
				
				// Increase chain multiplier
				score_multiplier += 100;
				
				// Chain matches
				playMove();
			}
			else
			{
				// No (more) chain matches
				
				_canSelect = true;
				
				// Check game over
				if( _gameOver)
				{
					gameOver();
					return;
				}
				
				// Find possible moves, and save one for a hint
				var hints:Array = new Array();
				findPossibleMoves(hints);
				
				if( hints.length > 0)
				{
					visuals.setHintGem(hints[Math.floor(Math.random() * hints.length)]);
				}
				else
				{
					// No moves remaining, game over
					gameOver();
					
					// (Another design option would be to shuffle)
				}
			}
		}
		
		private function checkAndRemove(gemCheck:Gem):int 
		{
			var matchesH:Array = new Array();
			var matchesV:Array = new Array();
			var matches:Array = new Array();
			
			// Check for matches in both vertical and horizontal directions
			checkMatch(gemCheck, matchesH, matchesV);
			
			// Horizontal matches?
			if( matchesH.length > 1)
				matches = matches.concat(matchesH);
			
			// Vertical matches?
			if( matchesV.length > 1)
				matches = matches.concat(matchesV);
			
			// Add gemCheck if there are matches
			if( matches.length > 1)
				matches.push(gemCheck);
			
			// Remove matches
			for each( var gem:Gem in matches)
			{
				visuals.explodeGem(gem);
				removeChild(gem);
				grid[gem.c][gem.r] = null;
			}
			
			return matches.length;
		}
		
		private function deselectGem():void
		{
			_gemSelected.scaleX = _gemSelected.scaleY = 1;
			_gemSelected = null;
			_gemSwap = null;
		}
		
		private function _swapGems(g1:Gem, g2:Gem, onCompleteFunction:Function = null):void
		{
			// Visual swap
			Tweener.addTween(g1, { x:g2.x, y:g2.y, time:Gem.SWAP_ANI_TIME, transition: "easeInOut"} );
			Tweener.addTween(g2, { x:g1.x, y:g1.y, time:Gem.SWAP_ANI_TIME, transition: "easeInOut", onComplete:onCompleteFunction} );
			
			// Logicial swap
			var swap_c:int = g2.c;
			var swap_r:int = g2.r;
			g2.c = g1.c;
			g2.r = g1.r;
			grid[g1.c][g1.r] = g2;
			grid[swap_c][swap_r] = g1;
			g1.c = swap_c;
			g1.r = swap_r;
		}
		
		private function _completeSwapAni():void
		{
			// Check matches
			var matchesN:int = 0;
			matchesN += checkAndRemove(_gemSelected);
			matchesN += checkAndRemove(_gemSwap);
			
			// Successful swap?
			if( matchesN > 0)
			{
				addScore(matchesN * score_multiplier);
				var note:ScoreNotification = new ScoreNotification(this, _gemSelected.x, _gemSelected.y, matchesN * score_multiplier);
				
				deselectGem();
				
				// Reset multiplier
				score_multiplier = 100;
				
				playMove();
			}
			else
			{
				// Not good, swap back!
				_swapGems(_gemSwap, _gemSelected, _completeSwapBackAni);
			}
		}
		
		private function _completeSwapBackAni():void 
		{
			_canSelect = true;
			deselectGem();
		}
		
		/** 
		 *  Check patterns:
		 *	   a     b     c		Key:
		 *	  O--   -O-   O.oo		 O = Gem to check
		 *	  .oo   o.o           	 . = Gem will move to
		 *	                     	 o = Matching gems
		 *	                     	 - = Irrelevant gem
		 */
		private function findPossibleMoves(movableGems:Array):uint
		{
			var g:Gem;
			var m:uint = 0;	// Moves counter
			
			for (var c:int = 0; c < COLS; c++)
			{
				for (var r:int = 0; r < ROWS; r++)
				{
					// Gem to check
					g = grid[c][r];
					
					if( checkPattern(g, 1, 1, 2, 1 , true, true)  		/* Pattern: a(x,y) */
					 || checkPattern(g, -1, 1, 1, 1 , true, false)		/* Pattern: b(x,y) */
					 || checkPattern(g, 2, 0, 3, 0, true, false) 		/* Pattern: c(x,y) */
					 || checkPattern(g, 1, 1, 1, 2 , true, true)  		/* Pattern: a(y,x) */
					 || checkPattern(g, 1, -1, 1, 1 , false, true)		/* Pattern: b(y,x) */
					 || checkPattern(g, 0, 2, 0, 3, true, false))		/* Pattern: c(y,x) */
					{
						movableGems.push(g);
						m++;
					}
				}
			}
			return m;
		}
		
		/** 
		 *  Check two gems that are offset from the provided gem are the same type
		 *   g: the gem to check
		 *   x1, y1, x2, y2: the offset position of a gem to check, relative to g
		 *   flipX: will also check the vertical reflection for the same pattern
		 *   flipY: will also check the horizontal reflection for the same pattern
		 *   flipX & flipY: will also check the vertical & horizontal reflection for the same pattern
		 */
		private function checkPattern(g:Gem, x1:int, y1:int, x2:int, y2:int, flipX:Boolean, flipY:Boolean):Boolean
		{
			var canMove:Boolean =    hasSameGem(g.type, g.c + x1, g.r + y1) && hasSameGem(g.type, g.c + x2, g.r + y2);	// Base pattern
			if( flipX)
				canMove = canMove || hasSameGem(g.type, g.c - x1, g.r + y1) && hasSameGem(g.type, g.c - x2, g.r + y2);	// Flip X
			if( flipY)
				canMove = canMove || hasSameGem(g.type, g.c + x1, g.r - y1) && hasSameGem(g.type, g.c + x2, g.r - y2);	// Flip Y
			if( flipX && flipY)
				canMove = canMove || hasSameGem(g.type, g.c - x1, g.r - y1) && hasSameGem(g.type, g.c - x2, g.r - y2);	// Flip X & Y
			
			return canMove;
		}
		
		private function hasSameGem(type:int, x_:int, y_:int):Boolean
		{
			return ( x_ >= 0 && x_ < COLS && y_ >= 0 && y_ < ROWS)			/* bounds check */
				&& ( grid[x_][y_] != null && grid[x_][y_].type == type);	/* gem type check */
		}
		
		public function addScore(newpoints:int):int
		{
			score += newpoints;
			visuals.txtScore.text = score.toString();
			return newpoints;
		}
		
		public function gameOver(fromCountdown:Boolean = false):void
		{
			// If called from FuseCountdown, but is already game over (ie from no more matches), then ignore
			if( fromCountdown && _gameOver)
				return;
			
			// If gems are dropping/chaining, then just set game over flag and return
			// Check flag when dropping/chaining has finished
			_gameOver = true;
			if( !_canSelect)
				return;
			
			var n:int = 0;
			for (var r:int = 0; r < ROWS; r++)
			{
				for (var c:int = 0; c < COLS; c++)
				{
					Tweener.addCaller(this, { delay:n * 0.01, count:1, onComplete:_removeAndExplodeGem, onCompleteParams:[Gem(grid[c][r])] } );
					n++;
				}
			}
			
			Tweener.addCaller(this, { delay:n * 0.01, count:1, onComplete:dispatchEventWith, onCompleteParams:[GAMEOVER, false] } );
			
			visuals.setHintGem();
			
			_canSelect = false;
		}
		
		private function _removeAndExplodeGem(gem:Gem):void
		{
			visuals.explodeGem(gem);
			grid[gem.c][gem.r] = null;
			removeChild(gem);			
		}
    }
}