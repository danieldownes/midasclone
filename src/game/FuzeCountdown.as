package game 
{
	import caurina.transitions.Tweener;
	import flash.events.GameInputEvent;
	import starling.core.Starling;
	import starling.textures.Texture;
	import starling.extensions.PDParticleSystem;
	
	public class FuzeCountdown 
	{
		private const COUNTDOWN_SECS:Number = 60;
		
		private var _game:GameControl;
		
		// Animation sequence, [time delay ratio, x, y]
		private var _sequence:Array = new Array( [0, 260,555], [0.1, 250,538],  [ 0.1, 255, 515], [ 0.2, 215, 506], [0.4, 205, 370], [0.2, 178, 372] );
		
		private var _mFuse:PDParticleSystem;
		
		public function FuzeCountdown(game_:GameControl) 
		{
			var fireConfig:XML = XML(new EmbeddedAssets.FireConfig());
			var fireTexture:Texture = Texture.fromBitmap(new EmbeddedAssets.FireParticle());
			_mFuse = new PDParticleSystem(fireConfig, fireTexture);
			
			_game = game_;
			
			game_.addChild(_mFuse);
			Starling.juggler.add(_mFuse);
		}
		
		public function reset():void
		{
			// Start
			_mFuse.emitterX = _sequence[0][1];
			_mFuse.emitterY = _sequence[0][2];
			_mFuse.start();
			
			var delayT:Number = 0;
			for( var n:uint = 1; n < _sequence.length; n++)
			{
				delayT += _sequence[n - 1][0] * COUNTDOWN_SECS;
				// Tween to next point in sequence
				Tweener.addTween(_mFuse,
					{ 
						emitterX:_sequence[n][1],
						emitterY:_sequence[n][2],
						delay:delayT,
						time:_sequence[n][0] * COUNTDOWN_SECS,
						transition: "linear",
						onComplete:(n == _sequence.length - 1 ? _fuseCompleted : null)
					} );
			}
		}
		
		private function _fuseCompleted():void
		{
			_game.gameOver(true);
			_mFuse.stop();
		}
		
	}

}