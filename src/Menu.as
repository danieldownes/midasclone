package  
{
	import flash.display.Scene;
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.utils.HAlign;
	
	public class Menu extends Sprite
	{
		public static const PLAY_GAME:String = "play_game";
		
		public function Menu(scene_:Sprite) 
		{
			var btnTexture:Texture = Texture.fromBitmap(new EmbeddedAssets.ButtonTexture());
			
			var button:Button = new Button(btnTexture);
			button.x = (scene_.width - button.width) * 0.72;
			button.y =  scene_.height * 0.4;
			button.scaleWhenDown = 0.97;
			addChild(button);
			
			var lblPlay:TextField = new TextField(100, 40, "PLAY", "ArialRoundGrad", 30);
            lblPlay.color = Color.WHITE;
            lblPlay.x = 50;
            lblPlay.y = 22;
			lblPlay.hAlign = HAlign.CENTER;
            button.addChild(lblPlay);
			button.addEventListener(TouchEvent.TOUCH, _onTouch);
		}
		
		private function _onTouch(e:TouchEvent):void
		{ 
			var touch:Touch = e.getTouch(this);
			
			if ( touch != null && touch.phase == TouchPhase.ENDED)
			{
				dispatchEventWith(PLAY_GAME, false);
			}
		}
		
	}
}